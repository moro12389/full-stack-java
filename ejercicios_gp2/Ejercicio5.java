
package ejercicios_gp2;

import java.util.Scanner;

public class Ejercicio5 {
    public static void main(String[] args) {
        /*
        
        5.Escribir un algoritmo que lea un valor n por teclado y muestre los primeros n t�rminos de la sucesi�n d        e Fibonacci.
        Tip: la sucesi�n de Fibonacci comienza con 0, luego 1 y a partir de all� cada nuevo n�mero es la suma            de los dos anteriores.
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un valor entero n para ver los valores en la serie de fibonacci: ");
        int num = s.nextInt();
        int a = 0;
        int b = 1;
        for (int i = 0; i < num; i++) {
            if (i == (num-1)){
                System.out.print(a );
            }else{
                System.out.print(a + ",");
            
            }
            int c=a+b;
            a=b;
            b=c;
            
            
        }
        System.out.println("");
    }
}
