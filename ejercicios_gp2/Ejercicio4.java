
package ejercicios_gp2;

import java.util.Scanner;

public class Ejercicio4 {
    /*
    
    4.Escribir un algoritmo que calcule el factorial de un n�mero ingresado por teclado.
    Tip: el factorial de un n�mero n es el resultado de multiplicar todos los n�meros enteros desde 1 hasta n.
    Por ejemplo, el factorial de 5 es 1 � 2 � 3 � 4 � 5 = 120.
    
    
    */
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un numero para calcular su factorial: ");
        int numero = s.nextInt();
        int factorial = 1;
        for (int i = 1; i <= numero; i++) {
            factorial = factorial * i;
        }
        System.out.printf("El facotrial de %s es %s %n",numero,factorial);
        
    }
}
