
package ejercicios_gp2;

import java.util.Scanner;

public class Ejercicio7 {
    /*
    
        7.Del listado de abajo, determinar qu� animal eligi� el usuario mediante la realizaci�n de tres preguntas        del tipo SI/NO acerca de las tres caracter�sticas elegidas (herb�voro, mam�fero, dom�stico). Mostrar el 
        resultado por pantalla.
    
    
    
    */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Piense en un animal y responda (si/no) las siguientes preguntas: ");
        System.out.println("Es un animal herbivoro?");
        String herbivoro = s.nextLine();
        System.out.println("Es un animal mamifero?");
        String mamifero = s.nextLine();
        System.out.println("Es un animal domestico?");
        String domestico = s.nextLine();
        if (herbivoro.equalsIgnoreCase("si")){
            if (mamifero.equalsIgnoreCase("si")){
                if (domestico.equalsIgnoreCase("si")){
                
                    System.out.println("Es un caballo");
                
                }else if(domestico.equalsIgnoreCase("no")){
                
                    System.out.println("Es un alce");
                
                }
                
            }else if(mamifero.equalsIgnoreCase("no")){
                if (domestico.equalsIgnoreCase("si")){
                
                    System.out.println("Es una Tortuga");
                
                }else if(domestico.equalsIgnoreCase("no")){
                
                    System.out.println("Es un caracol");
                
                }
            
            }
        }else if(herbivoro.equalsIgnoreCase("no")){
            if (mamifero.equalsIgnoreCase("si")){
                if (domestico.equalsIgnoreCase("si")){
                
                    System.out.println("Es un gato");
                
                }else if(domestico.equalsIgnoreCase("no")){
                
                    System.out.println("Es un Le�n");
                
                }
                
            }else if(mamifero.equalsIgnoreCase("no")){
                if (domestico.equalsIgnoreCase("si")){
                
                    System.out.println("Es una pit�n");
                
                }else if(domestico.equalsIgnoreCase("no")){
                
                    System.out.println("Es un c�ndor");
                
                }
            
            }
        
        }
    }
}
