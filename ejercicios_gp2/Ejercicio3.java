
package ejercicios_gp2;

import java.util.Arrays;
import static java.util.Arrays.sort;

public class Ejercicio3 {
    /*
    
    3. Escribir un programa que ordene un arreglo de n�meros enteros de
    manera ascendente.
    
    
    */
    public static void main(String[] args) {
        //Ordenamiento por inserci�n

        int[] myNum = {10, 2, 3, 18, 7, 0, 9, 11, 15};
        for (int i = 0; i < myNum.length; i++) {
            int pos=i;
            int aux=myNum[i];
            while((pos>0) && (myNum[pos-1]>aux)){
                myNum[pos] = myNum[pos-1];
                pos--;
                System.out.println(pos);
            }
            myNum[pos] = aux;
        }
        
        
        System.out.println(Arrays.toString(myNum));
        
    }
}
