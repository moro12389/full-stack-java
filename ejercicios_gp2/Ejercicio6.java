
package ejercicios_gp2;

public class Ejercicio6 {
    public static void main(String[] args) {
        /*
        
            6.Escribir un algoritmo que imprima el listado de los n�meros primos menores que 200. Aclaraci�n: el            n�mero 1 no es primo.
            Tip: un n�mero es primo si es divisible �nicamente por 1 y por s� mismo.
            Por ejemplo, el 7 es primo porque s�lo es divisible por 1 y por 7.
            El 6 no es primo porque es divisible por 1, por 2, por 3 y por 6.
        
        
        */
        
        for(int i=2;i<=200;i++){
            int count=0;
            for(int j=2;j<i;j++){

                if(i%j==0){
                    count++;
                
                }
            }
            if(count==0){
                System.out.println(i);
            
            }

        }
    }
}

