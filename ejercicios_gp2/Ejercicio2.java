
package ejercicios_gp2;


import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        /*
        
        2. Escribir un programa que lea una palabra por teclado y determine si
        es un palíndromo.
        Tip: los palíndromos son palabras que se leen igual de izquierda a
        derecha y viceversa. Por ejemplo, reconocer.
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese una palabra para saber si es palíndromo: ");
        String palabra = s.nextLine();
        char[] palabra_array = palabra.toCharArray();
        Boolean palindromo = true;
        for (int i = 0; i < palabra.length(); i++) {
          if (!(palabra_array[i] == palabra_array[(palabra.length()-(1+i))])){
              i  = palabra.length();
              palindromo = false;
          }
        }
        if (palindromo){
            System.out.println("Es palindromo");
        } else {
            System.out.println("No es palindromo");  
        }
    }
}
