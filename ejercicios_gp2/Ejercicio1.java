
package ejercicios_gp2;
import java.util.Scanner;

public class Ejercicio1 {
    /*
    
    1.Escribir un programa que reciba un n�mero entero por teclado. A continuaci�n, mostrar la tabla de multiplicar de ese n�mero.
    
    */
    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el valor del cual quiere obtener la tabla de multiplicar: ");
        int x = s.nextInt();
        
        for (int i = 0; i <= 10; i++) {
            System.out.println(x+"x"+i+"="+i*x);
        }
    }
}
