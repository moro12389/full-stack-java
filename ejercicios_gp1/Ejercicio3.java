package ejercicios_gp1;
import java.util.Scanner;






public class Ejercicio3 {
   
    public static void main(String[] args) {
         /*
    
    3.Escribir un programa que lea dos numeros y realice el calculo de la suma, la resta, la multiplicacion y la division entre ambos valores. Los resultados     deben mostrarse por pantalla.
    
    */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese un numero: ");
        float num1 = s.nextFloat();
        System.out.println("Ingrese otro numero: ");
        float num2 = s.nextFloat();
        System.out.println("La suma es: %s".formatted(num1 + num2));
        System.out.println("La multiplicacion es: %s".formatted(num1 * num2));
        System.out.println("La resta es: %s".formatted(num1 - num2));
        System.out.println("La division es: %s".formatted(num1 / num2));
    }
}
