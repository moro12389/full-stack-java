
package ejercicios_gp1;

import java.util.Scanner;

public class Ejercicio8 {
    public static void main(String[] args) {
        /*
        
        8.Pedir al usuario que ingrese una temperatura en grados Celsius y mostrar por pantalla su equivalente en kelvin y grados Fahrenheit. Las f�rmulas 
        para conversiones son:
        Kelvin = 273,15 + Celsius
        Fahrenheit = 1,8 � Celsius
        
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese una temperatura en C�: ");
        float temp = s.nextFloat();
        float kelvin =  273.15f + temp;
        float fahrenheit =(temp * 1.8f);
        System.out.println("El equivalente en �K y �F es : %s�K %s�F".formatted(kelvin,fahrenheit));
        
    }
}
