
package ejercicios_gp1;
import java.util.Scanner;


public class Ejercicio4 {
    public static void main(String[] args) {
        
        /*4.Escribir un programa que lea la estatura de tres personas, calcule el promedio de la altura de ellos y lo informe.*/
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la estatura de la persona 1: ");
        float persona1 =  s.nextFloat();
        System.out.println("Ingrese la estatura de la persona 2: ");
        float persona2 = s.nextFloat();
        System.out.println("Ingrese la estatura de la persona 3: ");
        float persona3 = s.nextFloat();
        System.out.println("El promedio de la altura de las 3 personas es: %.2f".formatted((persona1+persona2+persona3)/3));
        
    }
}
