package ejercicios_gp1;

import java.util.Scanner;

public class Ejercicio7 {
    public static void main(String[] args) {
        /*
        
        7.Escribir un programa que reciba el valor de dos edades y las guarde en dos variables. Luego el programa debe intercambiar los valores de ambas 
        variables y mostrarlas por pantalla. Por ejemplo, si el usuario ingres? los valores edad1 = 24 y edad2 = 35, el programa deber? mostrar edad1 = 35 y 
        edad2 = 24.
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese las edades de dos personas: ");
        System.out.println("Edad1: ");
        int edad1 = s.nextInt();
        System.out.println("Edad2: ");
        int edad2 = s.nextInt();
        int aux = edad1;
        edad1= edad2;
        edad2=aux;
        System.out.println("La edad de la primera persona es %s , y la de la segunda es %s".formatted(edad1,edad2));
    }
}
