package ejercicios_gp1;
import java.util.Scanner;

public class Ejercicio5 {
    public static void main(String[] args) {
      /*
        5.Pedir al usuario que ingrese el valor del radio de una circunferencia. Calcular y mostrar por pantalla el ?rea y el per?metro. Record? que el ?rea          y el per?metro se calculan con las siguientes f?rmulas:
        area = PI ? radio?
        perimetro = 2 ? PI ? radio
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el valor del radio de una circunferencia: ");
        float r = s.nextFloat();
        System.out.println("El area del circulo es: %.2f".formatted(Math.PI * Math.pow(r, 2)));
        System.out.println("El perimetro del circulo es: %.2f".formatted(2* Math.PI * r));
        
    }
}
