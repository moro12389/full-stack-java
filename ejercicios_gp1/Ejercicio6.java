package ejercicios_gp1;
import java.util.Scanner;


public class Ejercicio6 {
    public static void main(String[] args) {
        /*
        
        6.Pedir al usuario que ingrese el precio de un producto y el porcentaje de descuento. 
        A continuaci?n mostrar por pantalla el importe descontado y el importe a pagar.
        
        */
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el precio del producto: ");
        float precio = s.nextFloat();
        System.out.println("Ingrese el porcentaje de descuento: ");
        float descuento = s.nextFloat();
        float importeConDescuento = precio-(precio*descuento)/100;
        float importeDescontado = (precio*descuento)/100;
        System.out.println("El importe descontado fue de $%s y el monto total a pagar es de $%s".formatted(importeDescontado,importeConDescuento));
    }
}
