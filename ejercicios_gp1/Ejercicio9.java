package ejercicios_gp1;

import java.util.Scanner;





public class Ejercicio9 {
    /*
    
    9.A partir de una cantidad de pesos que el usuario ingresa a trav�s del teclado se debe obtener su equivalente en d�lares, en euros, en guaran�es y en re   ales. Para este ejercicio se consideran las siguientes tasas:
   1 d�lar = 231,68 pesos
   1 euro = 250,69 pesos
   1 peso = 31,00 guaran�es
   1 real = 46,81 pesos
   Tip: en este ejercicio se puede usar la funci�n printf y mostrar el resultado con dos decimales.
     
    */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de dinero en pesos que quiere convertir a euros,dolares,reales y guaranies: ");
        float pesos = s.nextFloat();
        float dolares = pesos/231.68f ;
        float euros = pesos/250.69f; 
        float guaranies = pesos*31.00f;
        float reales = pesos/46.81f;
        System.out.printf("En dolares : %.2f %n",dolares);
        System.out.printf("En euros : %.2f %n",euros);
        System.out.printf("En guaranies : %.2f %n",guaranies);
        System.out.printf("En reales : %.2f %n",reales);
        
    }
}
